# TinyUrl Clone 


TinyUrl的一个Clone

本来是照着`Cloning Internet Applications with Ruby`这本书做的样例，但感觉这本书实现的并不是很清楚。。于是自己简化了步骤，简单实现了功能。

如果想好好做的话，可以加入以下东西

- Admin界面，统计图表
- 数据库可以改用Redis？试试看，反正url的总量不大。
- 增加API接口
- 美化界面