require "sinatra"
require_relative "./url"


get '/:short_url' do 
	url = Url.first(:indentifier => params[:short_url])
	if url.nil?
		redirect to('/')
	else 
		redirect url.original, 301
	end
end


get '/' do 
	erb :index
end

post '/' do
	uri = URI::parse(params[:origin])
	custom = params[:custom].empty? ? nil : params[:custom]
	raise "Invalid URL" unless  uri.kind_of? URI::HTTP or uri.kind_of? URI::HTTPS
	@url = Url.shorten(params[:origin], custom)
	erb :index
end

