require 'data_mapper' 
DataMapper::Logger.new("db.log", :debug)
path = Dir.pwd
DataMapper.setup(:default, "sqlite://#{path}/db.sqlite")


class Url
	include DataMapper::Resource
	property :id, Serial
	property :original, String, :length => 255
	property :indentifier, String


	def self.shorten(original, custom=nil)
		url = Url.first(:original => original)

		return url if url

		if custom 
			raise 'Someone has already taken this custom URL, sorry' unless Url.first(:indentifier => custom).nil?
			url = Url.create(:original => original, :indentifier => custom)
		else 
			url = Url.create(:original => original)
			url.indentifier = url.id.to_s(36)
			url.save
		end
		return url
	end

end

DataMapper.finalize
DataMapper.auto_migrate!


